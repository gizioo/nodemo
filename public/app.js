var angularShowoff = angular.module('angularShowoff', ['ngAnimate']);
angularShowoff.factory('Step', () => {number=0;return {number:number}});
angularShowoff.factory('Age', () => {return {}});

angularShowoff.controller('mainController', ['$scope', '$rootScope', 'Age', 'Step', ($scope, $rootScope, Age, Step) => {
    const pages = ['new_web.html', 'old_web.html'];
    $rootScope.$watch(() => {
            return Step.number;
        },
        (newVal, oldVal) => { 
            if(newVal && newVal == 3){
                $scope.content = pages[1];
            }else{
                $scope.content = pages[0];
            }
        }
    );
    Step.number = 0;
    
    
}])

angularShowoff.controller('newController', ['$scope', '$http', '$window', '$timeout', 'Step', ($scope, $http, $window, $timeout, Step) => {
    $scope.delayVal = Step.number == 0 ? false : true;
    $scope.delayVal2 = Step.number == 0 ? false : true;
    $scope.delayVal3 = Step.number == 0 ? false : true;
    $scope.showNext = Step.number == 0 ? false : true;

    //timeouts for the first animations
    if(Step.number == 0 ){
        $timeout(() => { $scope.delayVal = true; }, 3000);
        $timeout(() => { $scope.delayVal2 = true; }, 4000);  
        $timeout(() => { $scope.delayVal3 = true; }, 5000);  
        $timeout(() => { $scope.nextStep() }, 6000);
    }else{
        $timeout(() => { $scope.nextStep() }, 0);
    }
    //A scope function to call in ngClick
    $scope.nextStep = () => {
        
        //Get from PAI
            Step.number += 1;
            $scope.showNext = false;
            $http.get('/api/steps/' + Step.number)
            .then(response => {
                //Type in the box
                $(".typed").typed({
                    strings: response.data.sentences,
                    typeSpeed: 10,
                    showCursor: false,
                    callback: () => {
                        $scope.showNext = Step.number >= 5 ? false : true;
                        //apply the value change beacuse we're in an anonymous callback!
                        $scope.$apply();
                    }
                });
            }).catch(response => {
                console.log('Error: ' + response.data);
            });
        
    }

}]);
//OLD STYLE (with 'function' and stuff)
angularShowoff.controller('oldController', ['$scope', 'Step', function($scope, Step){
    $scope.message = "Enter password to leave:";
    $scope.tries = 0;
    $scope.submitPassword = function(){
        $scope.tries += 1;
        if(CryptoJS.MD5($scope.password).toString() == "5f4dcc3b5aa765d61d8327deb882cf99"){
            Step.number+=1;
        }else{
            $scope.message = "INVALID password";
        }
    }

}]);
