var express = require('express'); 
var router = express.Router();

const data = require('./data.json');

router.route('/steps/:step_id')
    .get((req, res) => {///ES6 YEAAAAAAH!
    	const step = data.steps.find(s => s.id == req.params.step_id);
        res.json(step);
});

module.exports = router;
