// server.js

    // set up ========================
    var express  = require('express');
    var app      = express();                           
    var morgan = require('morgan');        
    var bodyParser = require('body-parser');    
    var router = require('./api/router');
    // configuration =================

    app.use(express.static(__dirname + '/public'));                 
    app.use(morgan('dev'));            
    app.use(bodyParser.urlencoded({'extended':'true'}));
    app.use(bodyParser.json());
 
    app.use('/api', router); 
    app.get('/old', (req, res) => {
        res.sendfile('./public/old_web.html');
    });
    app.get('*', (req, res) => {
        res.sendfile('./public/index.html');
    });
    app.listen(3000);


